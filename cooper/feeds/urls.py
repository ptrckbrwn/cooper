from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .views import FeedsView, FeedView, AddFeedView

urlpatterns = patterns('',
    url(r'^$', login_required(FeedsView.as_view()), name='feeds'),
    url(r'(\d+)/', login_required(FeedView.as_view()), name='feed'),
    url(r'^add/', login_required(AddFeedView.as_view()), name='add_feed'),
)
