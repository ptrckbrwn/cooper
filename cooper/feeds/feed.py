import feedparser
import uuid

from django.utils.html import strip_tags

from .models import Feed
from .helper import is_set


def parse_feed(feed):
    """
    Contruct an object that we can cleanly access from the view.

    """

    f = feedparser.parse(feed.url)
    articles = []

    for each in f['entries']:
        article = {
            "id": uuid.uuid1(),
            "title": each["title"],
            "url": each["link"],
            "date": each["published"],
            "summary": strip_tags(each["summary_detail"]["value"][:300]),
            "body": each["summary_detail"]["value"],
        }

        if is_set("author", each):
            article["author"] = each["author"]
        else:
            article["author"] = None

        articles.append(article)

    parsed_feed = {
        "id": feed.id,
        "title": f["feed"]["title"],
        "articles": articles,
    }

    return parsed_feed


def parse(user):
    """
    We dig up all the feeds that belong to a given user.

    """

    feeds = Feed.objects.filter(user=user)
    parsed_feeds = []

    for feed in feeds:
        parsed_feeds.append(parse_feed(feed))

    return parsed_feeds


def validate(url):
    """
    Validate if url works.
    @return success state as boolean

    """
    f = feedparser.parse(url)
    if len(f["entries"]):
        return True
    else:
        return False
