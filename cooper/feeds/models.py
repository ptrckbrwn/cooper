from django.db import models
from django.contrib.auth.models import User


class Feed(models.Model):
    """
    Our feed model. This is where

    """
    user = models.ForeignKey(User)
    url = models.URLField()

    def __unicode__(self):
        return (self.user.username + " - " + self.url)


class Article(models.Model):
    """
    How we keep track of read articles.

    """
    user = models.ForeignKey(User)
    feed = models.ForeignKey(Feed)
    url = models.URLField()
    read = models.BooleanField(default=False)

    def __unicode__(self):
        return (self.user.username + " - " + self.url)
