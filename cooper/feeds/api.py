from tastypie.resources import ModelResource
from .models import Feed


class FeedResource(ModelResource):
    class Meta:
        queryset = Feed.objects.all()
        resource_name = "feed"
