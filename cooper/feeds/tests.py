from django.test import TestCase

from .helper import is_set


class HelperTests(TestCase):
    def test_is_set(self):
        foo = {"cool": "beans"}
        result = is_set("cool", foo)
        self.assertEqual(result, True)

        result_two = is_set("bar", foo)
        self.assertEqual(result_two, False)
