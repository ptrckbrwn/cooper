from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import View

from .models import Feed
from .feed import parse, parse_feed, validate


class FeedsView(View):
    """
    Handles displaying articles in a lovely, readible format.

    """

    def get(self, request):
        try:
            feeds = parse(request.user)
        except Exception:
            return render(request, "feeds.html", {
                "error": "Could not load feeds.",
            })
        else:
            return render(request, "feeds.html", {"feeds": feeds})


class FeedView(View):
    """
    View for an individual feed.

    """

    def get(self, request, id):
        feed = Feed.objects.get(id=id)
        feed = parse_feed(feed)
        feeds = parse(request.user)
        return render(request, "feed.html", {
            "feeds": feeds,
            "feed": feed,
        })


class AddFeedView(View):
    """
    View for adding a feed to your feed list.

    """

    def get(self, request):
        return render(request, "add_feed.html")

    def post(self, request):
        feed_url = request.POST["feed_url"]
        user = request.user

        # Make sure feed is valid before adding to db
        if validate(feed_url):
            feed = Feed(user=user, url=feed_url)
            feed.save()
            return HttpResponseRedirect("/feeds/")
        else:
            return render(request, "add_feed.html", {
                "error": "URL was not a valid RSS or Atom feed.",
            })
