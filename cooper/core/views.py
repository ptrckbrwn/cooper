from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import View
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


class IndexView(View):
    """
    Our awesome home page! Handles user sign up.

    """

    def get(self, request):
        if request.user.is_authenticated():
            return HttpResponseRedirect("/feeds/")
        else:
            return render(request, "index.html")

    def post(self, request):
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']

        try:
            user = User.objects.create_user(
                username,
                email,
                password
            )
        except Exception:
            # Integrity Error
            return render(request, "index.html", {
                "error": "Username already taken.",
            })
        else:
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect("/feeds/")


class LoginView(View):
    """
    Handles logging in users.

    """

    def get(self, request):
        if request.user.is_authenticated():
            return HttpResponseRedirect("/feeds/")
        else:
            return render(request, "login.html")

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect("/feeds/")
            else:
                # Return a disabled account error message
                return render(request, "login.html", {
                    "error": "This account has been disabled.",
                })
        else:
            # Return an invalid login error message.
            return render(request, "login.html", {
                "error": "Username and password did not match.",
            })


class LogoutView(View):
    """
    Logs user out.

    """

    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/login/")
