from django.conf.urls import patterns, include, url
from feeds.api import FeedResource

from django.contrib import admin
admin.autodiscover()

feed_resource = FeedResource()


urlpatterns = patterns('',
    url(r'^', include('core.urls')),
    url(r'^feeds/', include('feeds.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(feed_resource.urls)),
)
