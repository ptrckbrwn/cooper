COOPER
======

*A Damn Fine RSS Reader™*


Objective
---------

COOPER is built around three goals:

1. Make reading RSS articles beautiful.
2. Make subscribing to RSS subscriptions easy.
3. Follow the *Zero Inbox Philosophy*.

Additionally, as code maintainers we promise to be [RESTFUL](),
[KISS](), and stay [DRY]().


Stories
-------

### New User

1. Go to cooper.io.
2. Fill out account info on index.
3. Click "sign up".
4. Redirected to a one-time tutorial splash page.
5. Redirected to empty feeds page.


### Returning User

- Already Logged in.
    1. Go to cooper.io.
    2. Redirected to feeds page.

- Not logged in.
    1. Go to cooper.io.
    2. Click "log in".
    3. Fill out credentials in modal.
    4. Click "log in".
    3. Redirected to feeds page.


### Adding a Feed

1. Go to cooper.io/feeds/.
1. Click "add feed".
2. Copy and paste feed url into field.
3. Feeds page now populates with added feed.


### Read an Article

1. Go to cooper.io/feeds/.
2. Click an article.
3. Redirected to article page.


License
-------

MIT.
