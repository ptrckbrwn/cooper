/**
 * gulpfile.js - cooper
 *
 * COMMANDS:
 * - serve: Serves django project at localhost:8000.
 * - test: Run test suite and print coverage.
 * - static: Collect static out of bower_components and "does the right thing."
 * - TODO prod: Do everything needed to groom cooper and then serve.
 */

var gulp = require('gulp');

var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-ruby-sass');
var notify = require('gulp-notify');
var exec = require('gulp-exec');

var paths = {
    scripts: {
        src: 'javascripts/**/*.js',
        dest: 'cooper/core/static/js'
    },
    styles: {
        watch: 'stylesheets/**/*.scss',
        src: 'stylesheets/main.scss',
        dest: 'cooper/core/static/css'
    }
};

/**
 * lint: lint javascript for errors.
 */
gulp.task('lint', function() {
    return gulp.src(paths.scripts.src)
        .pipe(jshint())
        .pipe(jshint.reporter('default')
    );
});

/**
 * scripts: minify and copy all JavaScript (except vendor scripts)
 */
gulp.task('scripts', function() {
    return gulp.src(paths.scripts.src)
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(notify({ message: "Scripts updated" })
    );
});

/**
 * styles: minify and concat sass files.
 */
gulp.task('styles', function() {
    return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(notify({ message: "Styles updated." }));
});

/**
 * watch: monitor sass and js files. Recompile and/or lint on change.
 */
gulp.task('watch', function() {
    gulp.watch(paths.styles.watch, ['styles']);
    gulp.watch(paths.scripts.src, ['lint', 'scripts']);
});

/**
 * server: start up a test server on localhost:8000.
 */
gulp.task('serve', function() {
    return gulp.src('/').pipe(exec('python cooper/manage.py runserver'));
});

/**
 * test: run tests with coverage.py
 */
gulp.task('test', function() {
    return gulp.src('/')
        .pipe(exec('coverage run --source="." cooper/manage.py test'))
        .pipe(exec('coverage report'));
});

/**
 * jquery: pull jquery of of bower_components.
 */
gulp.task('jquery', function() {
    return gulp.src('bower_components/jquery/dist/jquery.min.js')
        .pipe(gulp.dest(paths.scripts.dest));
});

/**
 * angular.
 */
gulp.task('angular', function() {
    return gulp.src('node_modules/angular/lib/angular.min.js')
        .pipe(gulp.dest(paths.scripts.dest));
});

/**
 * font-awesome.
 */
gulp.task('font-awesome', function() {
    return [
        gulp.src('bower_components/font-awesome/css/font-awesome.min.css')
            .pipe(gulp.dest(paths.styles.dest)),
        gulp.src('bower_components/font-awesome/fonts/*.*')
            .pipe(gulp.dest('cooper/core/static/fonts'))
    ];
});

/**
 * bootstrap-js.
 */
gulp.task('bootstrap-js', function() {
    return gulp.src('bower_components/bootstrap-sass/vendor/assets/javascripts/bootstrap/*.js')
        .pipe(uglify())
        .pipe(concat('bootstrap.min.js'))
        .pipe(gulp.dest(paths.scripts.dest));
});

/**
 * bootstrap-sass.
 */
gulp.task('bootstrap-sass', function() {
    return gulp.src('bower_components/bootstrap-sass/vendor/assets/stylesheets/bootstrap/*.scss')
        .pipe(gulp.dest('stylesheets/bootstrap'));
});

gulp.task('static', [
    'jquery',
    'angular',
    'font-awesome',
    'bootstrap-js',
    'bootstrap-sass'
]);
gulp.task('default', ['lint', 'scripts', 'styles', 'watch', 'serve']);
